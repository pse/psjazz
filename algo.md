# algorithm

- $n$ = number of scales 
- $m$​ = number of measures

scales: KS + mode
its accidentals are those of the KS + additional (lead degrees)



## 1. modal step 
objective:

- evaluation of one scale (KS + mode) for each measure and each initial scale.
  = construction of a table $n \times m$ of scales.



2 sub-steps:

- construction of a table $T$ of dim. $n \times m$ of integers
  $T[i, j]$ is the min. number of accidentals not in the scale $i$ for the spelling of measure $j$​.

- [ ] was also evaluated a list $L$ of candidate best globals. we skip this for now.
  
- construction of a *grid* $G$ of dim. $n \times m$ 
  $G[i, j]$ is the estimated scale for measure $j$, starting (in measure 0) with scale $i$.
  it is computed using a ranking of scales, which is the average of 3 rankings
  
  - ranking according to the values in column $j$ in table $T$ (smallest is best),
  - ranking by distance wrt $G[i, j-1]$ (of scale $i$ for $j = 0$)
  - ranking by distance wrt scale $i$​
  
  

## 2. tonal step

objective: 

- estimation of a global Key Signature
- spelling in this Key Signature, according to the scales in above grid $G$.

départager entre les lignes de $G$ obtenu à l'étape modale.



sub-steps:

- construction of a table $S$ of dim. $n \times m$ , where $S[i, j]$ is the best spelling of measure $j$  
  wrt cost-tuplets, ordered lexicographically, and made of 
  - number of accidentals printed, for the spelling of measure $j$  in the KS of $i$
    (NB: accidental printed ≠ accidentals not in the scale),
  - number of accidentals not in the scale $i$, 
  - number of accidentals not chromatic harmonic scale associated with $i$,
  - number of accidentals with a color different from the KS of $i$, 
  - number of Cb, B#, Fb, E#
- the KS is the KS of the row $i$ of $S$ with best cost (should be unique)
- the spelling is the spelling in the same row $i$ (i.e. the spelling with min. cost in this row)
- Moreover, the table $G$ associates one scale to each measure $j$ in this row $i$.



ordering go cost-tuplets

- lexico











