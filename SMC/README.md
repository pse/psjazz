# SMC 2024

https://smcnetwork.org/smc2024/
ESMAE, Porto, Portugal

- February 24th, 2024: Deadline for submitting an abstract and registering the paper music & sonic art on the conference portal (strict!).
- March 11nd, 2024: Deadline for paper / music & sonic art submission (strict!).
- March 30th, 2024: Deadline for submitting reviews.
- April 6th, 2024: Notification of acceptance.
- April 7th, 2024: Conference / Summer School registration open.
- May 11th, 2024: Camera-ready version due.
- May 18th, 2024: End of early-bird registration.
- July 4–6  : Conference



## submission

- maximum allowed length: 8 pages (for both lecture and poster presentations).
  a length of 6 pages is **strongly encouraged**.
- SMC template:
  https://smcnetwork.org/smc2024/SMC2024_templates.zip

- single-blind peer review
- submission site: 
  https://cmt3.research.microsoft.com/SMC2024

- Scientific Chairs: 
  - [Guilherme Campos](mailto:guilherme.campos@ua.pt)
  - [Inês Salselas](mailto:insc@esmae.ipp.pt)
  - [José Vieira](mailto:jnvieira@ua.pt)





