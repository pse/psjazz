% -----------------------------------------------
% Template for SMC 2024
% Adapted from previous SMC paper templates
% -----------------------------------------------
\documentclass{article}
\usepackage{smc2024}
%%%%%%%%%%%%%%%%%%%%%%%% Some useful packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%% See related documentation %%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[caption=false, font=footnotesize]{subfig}% Modern replacement for subfigure package
\usepackage{paralist}% extended list environments
\usepackage[figure,table]{hypcap}% hyperref companion

% The following command enables line numbers and it is meant for Review only
% it must be removed/commented out for Camera Ready version
\pagewiselinenumbers


% Use this if english is the only language/alphabet used in the document
%\usepackage[english]{babel}
%--------------------------------------
% Japanese, Chinese, Korean
\usepackage[whole]{bxcjkjatype}
%\usepackage[japanese, main=english]{babel} % Does not work with plain pdfTex

%--------------------------------------
% Cyrillic
% The support is not perfect, diacritics are not working
%\usepackage[T2A,T1]{fontenc}
%\usepackage[russian, main=english]{babel}
%--------------------------------------
% Greek
%\usepackage[LGR,T2A,T1]{fontenc}
%\usepackage[greek, russian, main=english]{babel}
\usepackage{alphabeta}
%--------------------------------------
% HIGHLY EXPERIMENTAL, DO NOT USE AS IS.
% Arabic, Farsi, and Hebrew
\usepackage{arabtex}
\usepackage[LFE,LAE,LGR,T2A,T1]{fontenc}
%\usepackage[farsi, arabic, greek, russian, main=english]{babel}
\usepackage[greek, russian, main=english]{babel}
%--------------------------------------
% If more languages are needed please add the relevant packages


% Title.
% ------
\def\papertitle{Paper Template for SMC 2024}

% Authors
% Please note that submissions are NOT anonymous, therefore 
% authors' names have to be VISIBLE in your manuscript. 
% Authors are entered as an ordered list, each one can be linked to multiple affiliations using the correct index.
% Available tags for authors are: \firstname \middlename \lastname \generation \originalname \email \orcid
% Available tags for affiliations are: \unit \department \institution \streetaddress \city \state \postcode \country \type
% type can take as value: University, Company, Music, Independent, Other
%
% \author[]{\mbox{\firstname{}\middlename{}\lastname{}\originalname{}\generation{}\email{}\orcid{}}}
% mbox force an author not to be split over multiple lines
\author[1]{\mbox{\firstname{Koji}\lastname{Kondo}\originalname{近藤 浩治}}}
\author[2]{\mbox{\firstname{Pyotr}\middlename{Ilyich }\lastname{Tchaikovsky}\originalname{\foreignlanguage{russian}{Пётр Ильич Чайковский}}}}
\author[3]{\mbox{\firstname{Iannis}\lastname{Xenakis}\originalname{\foreignlanguage{greek}\firstname{Γιάννης}\lastname{Ξενάκης}}}}
\author[4]{\mbox{\firstname{Davide Andrea}\lastname{Mauro}\email{maurod@marshall.edu}\orcid{0000-0001-8437-4517}}}
\author[1,2,3,4]{\mbox{\firstname{John}\lastname{Doe}\generation{3$^{\text{\itshape rd.}}$}}}

%Affiliations
\affil[1]{\institution{Nintendo Co., Ltd.}\city{Kyoto}\country{Japan}\affiliationtype{Company}}
\affil[2]{\institution{Saint Petersburg Conservatory}\city{St. Petersburg}\country{Russia}\affiliationtype{Music}}
\affil[3]{\department{Center for Mathematical and Automated Music}\institution{Indiana University Bloomington}\state{IN}\country{USA}\affiliationtype{University}}
\affil[4]{\department{Department Computer and Information Technology}\institution{Marshall University}\streetaddress{One John Marshall Dr.} \city{Huntington}\state{WV}\postcode{25755}\country{USA}\affiliationtype{University}}

% Complete setup stage
\completesetup

% Title.
% ------
\title{\papertitle}
% ***************************************** the document starts here ***************
\begin{document}
	%
	\capstartfalse
	\maketitle
	\capstarttrue
	%
	
	\begin{abstract}
		The abstract should be placed at the top left column and should contain about 150-200 words.
	\end{abstract}
	%
	
	\section{Introduction}\label{sec:introduction}
	This template includes all the information about formatting manuscripts for 
	the SMC 2024 Conference.
	Please, use \LaTeX{} templates when 
	preparing your submission.
	Please follow these guidelines to give the final proceedings a professional look.
	If you have any questions, you may contact the SMC 2024 organizers.
	This template can be downloaded from the SMC 2024 web site: 
	\url{https://smcnetwork.org/smc2024/}
	
	Changes for 2021 version (maintained in the 2024 version):
	\begin{itemize}
		\item Line numbering for Review purposes (Please turn it off for Camera Ready)
		\item Support of non latin alphabets for authors (see template for guidelines)
		\item Support of structured names for authors (firstname, middlename, lastname, generation, phonetic variants, email, orcid)
		\item Semistructured affiliation support (labunit, department, institution, streetaddress, city, state, postcode, country)
		\item Explicit mapping for affiliations and authors (Based on authoblk)
	\end{itemize}
	\section{Page size and format}
	\label{sec:page_size}
	The SMC 2024 proceedings will be formatted as portrait \underline{A4-size papers (21.0~cm x 29.7~cm)}. All material on each page should fit within a rectangle of 17.2~cm x 25.2~cm, centered on the page, beginning 2.0~cm from the top of the page and ending with 2.5~cm from the bottom. The left and right margins should be 1.9~cm. The text should be in two 8.2~cm columns with a 0.8~cm gutter. All text must be in a two-column format, and justified.
	The maximum allowed length is {\bf8 pages} (for both lecture and poster presentations). However, a length of {\bf6 pages} is \underline{strongly encouraged}.
	
	
	\section{Typeset Text}\label{sec:typeset_text}
	
	\subsection{Normal or Body Text}
	\label{subsec:body}
	Please use a 10~pt (point) Times font. Use sans-serif or non-proportional fonts only for special purposes, such as distinguishing source code.
	
	The first paragraph in each section should not be indented, 
	but all other paragraphs should be.
	
	\subsection{Title and Authors}
	The title is 16~pt Times, bold, caps, upper case, centered. Authors' names are centered. The lead author's name is to be listed first (left-most), and the co-authors' names after. If the addresses for all authors are the same, include the address only once, centered. If the authors have different addresses, put the addresses, evenly spaced, under each authors' name.
	
	\subsection{First Page Copyright Notice}
	Please leave the copyright notice exactly as it appears in the lower left-hand corner of the first page. It is set in 8~pt Times.
	
	\subsection{Page Numbering, Headers and Footers}
	Do not include headers, footers or page numbers in your submission. These will be added electronically at a later stage, when the publications are assembled.
	
	\section{Headings}
	First level headings are in Times 12~pt bold, centerd with 1 line of space above the section head, and 1/2 space below it.
	For a section header immediately followed by a subsection header, the space should be merged.
	
	\subsection{Second Level Headings}
	Second level headings are in Times 10~pt bold, flush left, with 1 line of space above the section head, and 1/2 space below it. The first letter of each significant word is capitalized.
	
	\subsubsection{Third Level Headings}
	Third level headings are in Times 10~pt italic, flush left, with 1/2 line of space above the section head, and 1/2 space below it. The first letter of significant words is capitalized.
	
	Using more than three levels of headings is strongly discouraged.
	
	
	
	
	
	\section{Floats and equations}
	
	\subsection{Equations}
	Equations should be placed on separated lines and numbered. The number should be on the right side, in parentheses.
	\begin{equation}
		r=\sqrt[13]{3}
		\label{eq:BP}
	\end{equation}
	Always refer to equations like this: ``Equation (\ref{eq:BP}) is of particular interest because...''
	
	\subsection{Figures, Tables and Captions}
	\begin{table}[t]
		\begin{center}
			\begin{tabular}{|l|l|}
				\hline
				String value & Numeric value \\
				\hline
				Moin! SMC & 2024 \\
				\hline
			\end{tabular}
		\end{center}
		\caption{Table captions should be placed below the table,  like this.}
		\label{tab:example}
	\end{table}
	
	All artwork must be centered, neat, clean and legible. Figures should be centered, neat, clean
	and completely legible. All lines should be thick and dark enough for purposes of reproduction. Artwork should not be hand-drawn. The proceedings will be distributed in electronic form only, therefore color figures are allowed. However, you may want to check that your figures are understandable even if they are printed in black-and-white.
	
	
	Numbers and captions of figures and tables always appear below the figure/table.
	Leave 1 line space between the figure or table and the caption.
	Figure and tables are numbered consecutively. 
	Captions should be Times 10pt. Place tables/figures in the text as close to the reference as possible, 
	and preferably at the top of the page.
	
	Always refer to tables and figures in the main text, for example: ``see Fig. \ref{fig:example} and \tabref{tab:example}''.
	Figures and tables may extend across both columns to a maximum width of 17.2cm.
	
	Vectorial figures are preferred, e.g., eps. When using \texttt{Matlab}, export using either (encapsulated) Postscript or PDF format. In order to optimize readability, the font size of text within a figure should be no smaller than
	that of footnotes (8~pt font-size). If you use bitmap figures, make sure that the resolution is high enough for print quality. 
	
	\begin{figure}[t]
		\centering
		\includegraphics[width=0.6\columnwidth]{figure}
		\caption{Figure captions should be placed below the figure, 
			exactly like this.\label{fig:example}}
	\end{figure}
	
	
	\subsection{Footnotes}
	You can indicate footnotes with a number in the text \footnote{This is a footnote example.},
	but try to work the content into the main text.Use 8~pt font-size for footnotes.  Place the footnotes at the bottom of the page 
	on which they appear. Precede the footnote with a 0.5~pt horizontal rule.
	
	\section{Citations}
	All bibliographical references should be listed at the end, inside a section named ``REFERENCES''. References must be numbered in order of appearance. You should avoid listing references that do not appear in the text.
	
	Reference numbers in the text should appear within square brackets, such as in~\cite{Someone:00} or~\cite{Someone:00,Someone:04,Someone:09}. The reference format is the standard IEEE one. We highly recommend you use BibTeX 
	to generate the reference list.
	
	\section{Conclusions}
	Please, submit full-length papers. Submission is fully electronic and automated through the Conference Web Submission System. \underline{Do not} send papers directly by e-mail.
	
	
	\begin{acknowledgments}
		At the end of the Conclusions, acknowledgements to people, projects, funding agencies, etc. can be included after the second-level heading  ``Acknowledgments'' (with no numbering).
	\end{acknowledgments} 
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%bibliography here
	\bibliography{smc2024bib}
	
\end{document}
