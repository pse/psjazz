# Pitch Spelling for jazz modes

suite projet algo PSE

https://gitlab.inria.fr/pse/psjazz



cf. 

- code PSE ~~and PS13b~~
  https://gitlab.inria.fr/pse/pse
- evaluation https://gitlab.inria.fr/pse/pseval
  - Real Book (100 opus)

  - FiloBass
  
  - FiloSax
  
- article:
  https://gitlab.inria.fr/pse/psjazz





## Questions



- utilité des modes jazz / maj. et mineur harmonique

→ contre-exemple: scholastique ou réel

→ résultats d'évaluation



- qualité des datasets

→ choix de KS durant la curation

→ choix des spelling durant la curation



- algo rapide mais partiel à la PS14

→ fiabilité sur PS jazz?

→ comparer les résultats d'évaluation avec

- PSE135
- PSE30
- PS14



## TODO



- [x] gitlab `PSjazz`



- [x] trouver un dataset partitions jazz
  cf.  [`datasets.md`](datasets.md)

- standards (Real Book) 
- ou transcriptions soli / basslines



- [x] pybind PSE: définition de 2 `TonIndex` 

  - 1 pour PS Table (Tonalité locale)

  - 1 pour PS Grid (Tonalités locales)



- [x] code PSE, classe `Ton` :
  ajouter les grilles manquantes pour les modes heptatoniques



- [x] distance entre `Ton` (avec modes jazz)
  → généralisation de table de Weber
  
  - cf. dans la classe `Ton`  (à tabuler ?)
    - `distFifths`  (Diletski)
    - `distHamming`
    - `distDiatonic` qui est un pointwise `accidDist`
  
  - refs distance tonale: [`distanceton.md`](distanceton.md)



## TODOplus



- revoir les bornes pour changement de tonalité locale
  en utilisant les états



- évaluation tonalités locales estimées avec accords de *lead-sheet*



