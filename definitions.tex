% !TEX root = PSJazz.tex

In this section, we describe the problem studied in the paper 
and introduce a new distance between keys which 
is one key component of our method for this problem.
%ingredient that we are using / introducing

\subsection{Problem Input}\label{sec:input}
We assume given input
a  sequence of notes $\nu_1,\ldots, \nu_p$, %organized in measures (bars),
called a \emph{part}.
It shall typically represent one staff in music notation,
with polyphonic content made of possibly several voices and chords.

Each note in input is characterized by a MIDI \emph{pitch}~\cite{MIDI}, % value, 
which is a natural number in $0..128$ corresponding to 
the distance in semitones
from a reference lowest note.% (the MIDI value 0).

In the following, we shall reason modulo octave, 
and will consider MIDI pitches modulo~12, called \emph{pitch classes}.
The pitch class of a note~$\nu$ is denoted by $\pc(\nu)$.
%The onsets and durations of the notes $\nu_1,\ldots, \nu_p$ are assumed 
%to be theoretical timings of music notation, and not timings of performances.

Regarding the timings of input notes, 
we assume a partial ordering relation $\prec$ on $\nu_1,\ldots, \nu_p$, 
such that $\nu_i \prec \nu_j$ implies that $i < j$.
Two notes $\nu$ and $\nu'$ 
incomparable \wrt $\prec$ are called \emph{simultaneous}, 
which we denote by $\nu \sim \nu'$.
%
That will be the cases of notes with the same onset, 
because they are involved in the same chord, 
or notes in different voices and starting simultaneously.
%
However, a \emph{grace-note} $\nu$, 
%a note of theoretical duration zero. 
be it single or involved in %part of 
an ornament (appoggiatura, gruppetto, mordent, trill \etc)
is not considered simultaneous with the next ordinary note~$\nu'$, 
but preceding it, \ie~$\nu \prec \nu'$, although in a score, 
$\nu$ and~$\nu'$ have theoretically the same onset.
 
We assume moreover that the input notes  
are organized into measures (we use the term of \emph{bars} in the following), 
and that the bar limits are given along with 
the input sequence $\nu_1,\ldots, \nu_p$.

\medskip\noindent
More precisely, to summarize, we assume given in input 
a sequence $\nu_1,\ldots, \nu_p$ such that every note $\nu_i$ is defined by: 
\begin{itemize}
\item a MIDI pitch value in $0..128$, 
\item a boolean flag expressing whether it is simultaneous with the next note or not, 
\ie whether $\nu_i \sim \nu_{i+1}$, 
\item a boolean flag expressing whether $\nu_i$ belongs to the same bar as $\nu_{i+1}$.
\end{itemize}
By convention, the two flags are set to \emph{false} for $\nu_p$.

%\begin{example}
%...
%\end{example}


Although the above information could be extracted from a
MIDI file recording a performance, by mean of some heuristics, 
our approach is more adquate to notated music information, 
with exact (quantized) timings.
Hence, it is particularly relevant for an usage in music transcription
(conversion of performed MIDI data into a score), 
\eg as a backend of a rhythm quantization procedure.


\subsection{Note Names}
The spelling of a note is made of:
\begin{itemize}
\item 
a note \emph{name} in $\Ap.. \Gp$,
%
\item 
a symbol of \emph{accidental}, amongst 
\becarre\xspace (natural), 
\bemol\xspace (flat), 
\doublebemol\xspace (double flat), 
\diese\xspace (sharp), 
\doublediese\xspace (double sharp), 
%
\item 
an \emph{octave number} in $-2.. 9$.
\end{itemize}
%
Every note name is associated a unique pitch class: 
0 for~$\Cp$ up to~11 for~$\Bp$, 
%Hence, the pitch class~0 corresponds to $\Cn$ and 
%the pitch class~11 corresponds to $\Bn$.
and the accidental symbol acts as a pitch class modifier: 
\doublebemol, 
\bemol, 
\becarre, 
\diese, 
\doublediese, 
respectively add -2, -1, 0, 1, and 2 to the pitch class 
of the note name component in a spelling.
%
Moreover, the note of (lowest) MIDI pitch $0$
corresponds to $\Cp${-1}. 
%
Altogether, every spelling as above can be associated a unique MIDI pitch value.
For instance, $\Gs\,9$ corresponds to the highest MIDI pitch 128,
and the extreme notes~$\Ap 0$ and~$\Cp 7$ of the 88 keys of a piano correspond
to the respective MIDI pitches~21 and~96.

On the opposite, the correspondence is not 1-1: 
there are several (2 or 3) alternative valid spellings for a given MIDI value, 
%Therefore, every pitch class can be denoted by two or three  spellings,  
as summarized in Figure~\ref{fig:enharmonics} for the 12 pitch classes.
%
For instance, $\Bs${-2} and $\Dp\doublebemol${-1}
are alternative spellings for the MIDI pitch 0.
%
For a every pitch class, the 2 or 3 alternative spellings 
of Figure~\ref{fig:enharmonics} have different names.
Hence, choosing a spelling for 
a given MIDI pitch MIDI value $m$ 
amounts to select one of the 2 or 3 possible names for $m$ modulo 12
(the accidental and octave can then be deduced from the name chosen for $m$).

\begin{figure}
\begin{center}
\begin{tabular}{|c|ccc|}
\hline
pitch class & spelling$_1$ & spelling$_2$ & spelling$_3$\\
\hline
  0 & $\Dff$   & $\Cp$ & $\Bs$ \\
  1 & $\Df$    & $\Cs$ & [$\Bss$] \\
  2 & $\Eff$   & $\Dp$ & $\Css$ \\
  3 & $[\Fff]$ & $\Ef$ & $\Ds$ \\
  4 & $\Ff$    & $\Ep$ & $\Dss$ \\
  5 & $\Gff$   & $\Fp$ & $\Es$ \\
  6 & $\Gf$    & $\Fs$ & [$\Ess$] \\
  7 & $\Aff$   & $\Gp$ & $\Fss$ \\
  8 & $\Af$    & $\Gs$ &        \\
  9 & $\Bff$   & $\Ap$ & $\Gss$ \\
 10 & [$\Cff$] & $\Bf$ & $\As$ \\
 11 & $\Bf$    & $\Bp$ & $\Ass$ \\
 \hline
 \end{tabular}
\end{center}
\caption{Enharmonic spellings for each pitch class.}
\label{fig:enharmonics}
\end{figure}

%\begin{example} \label{ex:running}
%running example...
%explaining a spelling choice by signature and scale...
%\end{example}

	
\subsection{Key Signature and Scales}\label{sec:scales}
A \emph{Key Signature} (KS) is an integral number $k$ between $-7$ and $7$, 
which indicates that by default, 
$|k|$ note names shall be altered 
by a \diese{},  % sharp accidental symbol 
when $0 < k \leq 7$,  
or by a \bemol{}, %\xspace % flat accidental 
when $-7 \leq k < 0$.
%
The names of the notes altered are defined according to the order of fifths: 
$\Fs, \Cs, \Gs, \Ds, \As, \Es, \Bs$ for the sharps ($k$ positive)
and 
$\Bf, \Ef, \Af, \Df, \Gf, \Cf, \Ff,$ for the flats ($k$ negative).
%
In a score, a KS indication is placed at the beginning of a part and will
influence the display of the spelling of notes in every measure,
as described in Section~\ref{sec:convention}.
The KS may be changed during a part.

A \emph{mode} is a sequence of intervals.
In this work, we consider the following 9 diatonic modes, made of 7 intervals:
ionian = major mode,
harmonic minor, 
melodic minor, 
dorian ($\Dp$ mode),
phrygian ($\Ep$ mode),
lydian ($\Fp$ mode),
mixolydian ($\Gp$ mode),
aeolian = natural minor ($\Ap$ mode) 
and locrian ($\Bp$ mode).

We call \emph{scale} the combination of a KS and a mode.

Each scale can be associated some accidentals, outside the KS, 
that we call \emph{characteristics accidentals}.
Intuitively, these accidental, when printed, enable to recognize the scale at sight.
This presentation is depicted in extenso, for all scales considered in this paper, 
in Figure~\ref{fig:scales} in Appendix.

%\begin{example} 
%\end{example}

	
\subsection{Distance between Scales}	
\label{sec:Weber}
... table in Figure~\ref{fig:distance}...TBC


the distance between is the smallest path between them 
in the table, following steps in the three axes.

\input{fig-Weber}

\subsection{Problem Output}\label{sec:output}
Given input notes, in the form presented in Section~\ref{sec:input},
we are expecting the following outcome:
\begin{itemize}
\item An estimated global KS. 
\item Spellings for the notes, in the chose KS.
\item One estimated local scale for each measure.
\end{itemize}
The local scales are more a byproduct of our algorithm, 
use in an intermediate state to estimate the best spellings.



%\begin{example} \label{ex:running}
%running example...
%KS and scale info
%\end{example}


