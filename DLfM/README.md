# DLfM'24

https://dlfm.web.ox.ac.uk

- june 27, 2024
  Stellenbosch University Konservatorium in South Africa
- In association with (IAML) -  23-28 June 2024
  the annual conference of the International Association of Music Libraries, Archives, and Documentation Centres 
- theme: *Musicology of the Global South: The Role of Digital Libraries*

- deadline (extended):  February 22nd, 2024

- [ ] question: hybrid or physical conference ?



## submission

https://easychair.org/conferences/?conf=dlfm2024

- full papers (up to 8 pages excluding references) 
- short papers (up to 4 pages excluding references)
- double-blind 
- ACM template for **two-column** papers
  with the `sigconf`, `authordraft`, and `anonymous` settings for initial submission





## posters

- deadline April 22nd, 2024

- abstract which outlines both the scholarly content and broad details of the proposed layout in 500 words or fewer. 
- [dlfm2024@easychair.org](mailto:dlfm2024@easychair.org)
- hybrid or physical ?











