% -----------------------------------------------
% Template for ISMIR Papers
% 2025 version, based on previous ISMIR templates

% Requirements :
% * 6+n page length maximum
% * 10MB maximum file size
% * Copyright note must appear in the bottom left corner of first page
% * Clearer statement about citing own work in anonymized submission
% (see conference website for additional details)
% -----------------------------------------------
\DocumentMetadata{testphase={phase-III,activate=tagging}}
\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[submission]{ismir} % Remove the "submission" option for camera-ready version
\usepackage{amsmath,cite,url}
\usepackage{graphicx}
\usepackage{color}

% Title. Please use IEEE-compliant title case when specifying the title here,
% as it has implications for the copyright notice
% ------
\title{Pitch Spelling Jazz Lead Sheets and Soli Transcriptions}

% Note: Please do NOT use \thanks or a \footnote in any of the author markup

% Single address
% To use with only one author or several with the same address
% ---------------
\oneauthor
  {Anonymous Authors}
  {Anonymous Affiliations\\\texttt{anonymous@ismir.net}}

% Two addresses
% --------------
%\twoauthors
%   {First author} {School \\ Department}
%   {Second author} {Company \\ Address}

%\author[1]{\mbox{\firstname{Augustin}\lastname{Bouquillard}\email{augustin.bouquillard@polytechnique.edu}}}
%\author[2]{\mbox{\firstname{Florent}\lastname{Jacquemard}\email{florent.jacquemard@inria.fr}}}
%\author[3]{\mbox{\firstname{John}\middlename{Xavier}\lastname{Riley}\email{j.x.riley@qmul.ac.uk}\orcid{0000-0001-8437-4517}}}

% Affiliations
% École Polytechnique
% \affil[1]{\institution{Institut Polytechnique de Paris}\city{Palaiseau}\country{France}\affiliationtype{University}}
% https://jacquema.gitlabpages.inria.fr
% \affil[2]{\institution{Inria Paris \& CNAM}\city{Paris}\country{France}\affiliationtype{University}}
% School of Electronic Engineering and Computer Science
% \affil[3]{\department{Centre for Digital Music}\institution{Queen Mary University London}\city{London}\country{UK}\affiliationtype{University}}

% Three addresses
% --------------
% \threeauthors
%   {First Author} {Affiliation 1 \\ \texttt{author1@ismir.edu}}
%   {Second Author} {Affiliation 2 \\ \texttt{author2@ismir.edu}}
%   {Third Author} {Affiliation 3 \\ \texttt{author3@ismir.edu}}

% Four or more addresses
% OR alternative format for large number of co-authors
% ------------
% \multauthor
%   {First author$^1$ \hspace{1cm} Second author$^1$ \hspace{1cm} Third author$^2$}
%   {{\bf Fourth author$^3$ \hspace{1cm} Fifth author$^2$ \hspace{1cm} Sixth author$^1$}\\
%   $^1$ Department of Computer Science, University, Country\\
%   $^2$ International Laboratories, City, Country\\
%   $^3$ Company, Address\\
%   {\tt\small CorrespondenceAuthor@ismir.edu, PossibleOtherAuthor@ismir.edu}
%   }

% For the author list in the Creative Common license, please enter author names.
% Please abbreviate the first names of authors and add 'and' between the second to last and last authors.
\def\authorname{}
%\def\authorname{A. Bouquillard, and F. Jacquemard}

% Optional: To use hyperref, uncomment the following.
% \usepackage[bookmarks=false,pdfauthor={\authorname},pdfsubject={\pdfsubject},hidelinks]{hyperref}
% Mind the bookmarks=false option; bookmarks are incompatible with ismir.sty.

\sloppy % please retain sloppy command for improved formatting


%%%%%%%%%%%%%%%%%%%%%%%% Other packages %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amsmath} % popular packages from Am. Math. Soc. Please use the 
\usepackage{amssymb} % related math environments (split, subequation, cases,
\usepackage{amsfonts}% multline, etc.)
\usepackage{bm}      % Bold Math package, defines the command \bf{}

% theorem-like environments
\usepackage{amsthm}  
\theoremstyle{definition}
\newtheorem{example}{Example}[section]

% urls
\usepackage{url}
\renewcommand\UrlFont{\rmfamily} % font for url

% colors
\usepackage{color}

% CSV tables
%\usepackage{csvsimple}
%\usepackage[bookmarks=false,hidelinks]{hyperref}
% bookmarks=false option; bookmarks are incompatible with ismir.sty.

% pdf with several pages
%\usepackage{pdfpages}

% change number of columns
%\usepackage{dcolumn}
%\usepackage{multicol}

% pictures
\usepackage{tikz}

% Comments: package todonotes
% options
% - disable: hide todos (use for final version)
% - obeyDraft: enable todonotes only if option draft, draftcls or draftclsnofoot is given in documentclass
% - obeyFinal: same for final option
% - textsize=value e.g.  textsize=tiny. default is textsize=normalsize
% - tickmarkheight=length default 0pt
% - textwidth=length
% - prependcaption: for option caption=val in the todo command%\usepackage[prependcaption,colorinlistoftodos,tickmarkheight=5pt]{todonotes} 
\usepackage[prependcaption,colorinlistoftodos,textwidth=14mm]{todonotes} 
%\usepackage[disable]{todonotes} 
% option disable to hide todos 
% option obeyDraft to display todo notes only in draft option (in documenclass)
% command todo : options
% - noline: no line connecting the note with the place in the text where the note occurs in the latex code
% - fancyline : curved arrow from note to insertion point
% - inline: place a todonote inside the text instead of in the margin
% - author=name
% - caption={name}
%\setuptodonotes{noline,size=\tiny}
%\setlength{\marginparwidth}{0.5in}
\newcommand{\florent}[1]{\todo[noline,size=\tiny,color=yellow!40,caption=Fl]{#1}}
\newcommand{\aug}[1]{\todo[noline,size=\tiny,color=blue!12,caption=Aug]{#1}}
%\newcommand{\listTODO}{\newpage\listoftodos}
%\newcommand{\florent}[1]{}
%\newcommand{\augustin}[1]{} % Highlight text.
%\newcommand{\listTODO}{}


% misc macros
\input{macros}


% ***************************************** the document starts here ***************
\begin{document}

\maketitle

\begin{abstract}
We present an algorithm of Pitch Spelling tailored for written jazz music.
Receiving some input in a MIDI-like format, that includes information about note heights (expressed in semitones from a reference lowest note) and boundaries of measures, it estimates jointly one global key signature (KS), appropriate note names and one local scale for each measure.

These values are assessed jointly in two optimization steps. In a first "modal" step, one likely scale is guessed for each measure, by minimizing the number of accidentals that would be printed in the engraved score. Then, in a second "tonal" step, these local scales are used for estimating the key signature that would give the best note spelling.

We report experiments with a set of lead sheets from the Real Book (in MusicXML format) and some transcriptions of Jazz soli recordings from the Weimar Jazz Database (in MIDI format).

Our procedure was originally designed for an application to music transcription, in particular the construction of digital collections of written jazz soli from audio recordings, in the context of teaching and cultural heritage preservation.
Moreover, it is based on considerations on distances between jazz scales which could be of some interest in musicological studies.
\end{abstract}
	
	
\section{Introduction}\label{sec:introduction}
\input{introduction}


% Definitions	
\section{Names and Scales}\label{sec:prelim}
\input{definitions} 

	
% Algo
\section{Pitch Spelling Algorithm}\label{sec:algo}\label{sec:method}
\input{algo}
	
% Exp
\section{Evaluation}
\input{evaluation}
	
\section{Conclusion}

	
% For BibTeX users:
\bibliography{references}


%%
%% If your work has an appendix, this is the place to put it.
%\clearpage
%\onecolumn
%\appendix
%%\appendixpage  \usepackage{appendix}
%\pagenumbering{alph}

%\section{Appendix}
%...
%
%\subsection{Scales}
%Section~\ref{sec:scales}
%\begin{figure}
%\input{scales.tex}
%\caption{Scales defined by a KS and characteristic accidentals.
%         KS' is the princted key signature. The tonic is displayed in parentheses.}
%\label{fig:scales}
%\end{figure}

	
\end{document}
