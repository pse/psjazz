# Jazz datasets

datasets with XML music scores and annotations



## voir aussi

autres pistes à explorer:

- https://ismir.net/resources/datasets/
- discussion liste ISMIR:
  https://groups.google.com/a/ismir.net/g/community/c/jx5XLvfSJ8A/m/AmKroUpVz6QJ
  cf. paragraphe BIAB below
- JAZZ VAR below: 
  (the authors have collected 234 jazz standards from the Fake Real Book)



## Weimar Jazz Database

- 456 manually transcribed solos 
- by 78 performers
- 1925-2009

> M. Pfleiderer, K. Frieler, J. Abeßer, W.-G. Zaddach, and B. Burkhart
> Inside the Jazzomat: New Perspectives for Jazz Research. 
> Schott Music GmbH, 2017.

sources:
- http://mir.audiolabs.uni-erlangen.de/jazztube/downloads
- Jazzomat site: https://jazzomat.hfm-weimar.de/download/download.html#weimar-jazz-database
- DB format: https://jazzomat.hfm-weimar.de/dbformat/dboverview.html
- [https://www.frontiersin.org/articles/10.3389/fdigh.2018.00001/full](https://www.frontiersin.org/articles/10.3389/fdigh.2018.00001/full)  

content: SQLITE3
- scores in pdf
- audio
- chord grids
- solo notes in CSV (onset, dur, pitch)
- beats in CSV (nb, onset)
- annotations about idiom
- annotations about style
- annotations about form 

pas de nom de notes,  mais [pitchs MIDI](https://jazzomat.hfm-weimar.de/melospy/transformations.html#trans-pc) et [Chordal Diatonic Pitch Class](https://jazzomat.hfm-weimar.de/melospy/transformations.html#trans-cdpc)


tools IE:
- melconv
  convertion of encodings (including jazzomat SV files, SQLITE3)
- [melfeature](https://jazzomat.hfm-weimar.de/tutorials/melfeature/melfeature_tut_intro.html) : extraction caractéristiques de mélodies (command line tool)
- meloSpy (GUI du précédent)
- [melpat](https://jazzomat.hfm-weimar.de/commandline_tools/melpat/melpat.html) : extraction et recherche de patterns (command line tool)



## Dig That Lick - DTL1000 dataset

- automated transcriptions 
- 1750 solos 
- from 1060 tracks

> L. Henry, K. Frieler, G. Solis, M. Pfleiderer, S. Dixon, F. Höger, T. Weyde, and H.-C. Crayencour
> Dig that lick: Exploring patterns in jazz with computational methods
> Jazzforschung/Jazz Research, Vol. 50, 2020.

> K. Frieler, F. Höger, M. Pfleiderer, and S. Dixon
> Two web applications for exploring melodic patterns in jazz solos
> in 19th International Society for Music Information Retrieval Conference. 
> ISMIR, 2018, pp. 777– 783.


sources: 
- project http://dig-that-lick.eecs.qmul.ac.uk
- https://dml.city.ac.uk



## WikiFonia

- 6,675 lead sheets 

sources:
- https://en.wikipedia.org/wiki/Wikifonia  (fermé pour pbs copyrights)
- files: 
	- https://www.dropbox.com/sh/11lv6srce2627gb/AAD5Z_L8qbbTYbGLGz-5MqT0a?dl=0
	  
	- [http://www.synthzone.com/files/Wikifonia/Wikifonia.zip](http://www.synthzone.com/files/Wikifonia/Wikifonia.zip)
	   **téléchargé**
	- [https://www.sugarsync.com/pf/D7389688_4167795_9692714](https://www.sugarsync.com/pf/D7389688_4167795_9692714)
	  **404**



content:

- scores in mxl format



## BIAB

Band in a Box songs

sources:
- http://nortonmusic.com/page-a-f.html

- files: https://www.dropbox.com/sh/11lv6srce2627gb/AAD5Z_L8qbbTYbGLGz-5MqT0a?dl=0
  **téléchargé: **

  - **fichiers BIAB, aux formats** 
    - **MXML**,  
  
    - **MSCZ**,  
  
    - **PDF**
  
- cf. ISMIR discussion list

  https://groups.google.com/a/ismir.net/g/community/c/jx5XLvfSJ8A/m/xu3-azM3bnwJ



see also:
[[Blues or Jazz datasets]]: Simon Dixon: _There's a subset of the real book extracted from Band-in-a-Box files which we wrote about here:_

> M. Mauch, S. Dixon, C. Harte, M. Casey, B. Fields
> [Discovering Chord Idioms Through Beatles and Real Book Songs](http://www.eecs.qmul.ac.uk/%7Esimond/pub/2007/ISMIR2007_p255_mauch.pdf).
> Proceedings of the [8th International Conference on Music Information Retrieval (ISMIR 2007)](http://ismir2007.ismir.net/)

no details about accessibility in the paper:
_The [...] collection is a transcription of the chords of 244 Jazz standards from the Real Book [12]._



conversions 

- https://keunwoochoi.wordpress.com/2016/02/19/lstm-realbook/

* https://keunwoochoi.wordpress.com/2016/04/20/paper-is-out-text-based-lstm-networks-for-automatic-music-composition/



## Pachet Jazz DB

www.flow-machines.com/lsdb (404)


> François Pachet, [Jeff Suzda](http://dblp1.uni-trier.de/pers/hd/s/Suzda:Jeff), [Dani Martínez](http://dblp1.uni-trier.de/pers/hd/m/Mart=iacute=nez:Dani)
> [A Comprehensive Online Database of Machine-Readable Lead-Sheets for Jazz Standards](https://www.francoispachet.fr/wp-content/uploads/2021/01/pachet-13c.pdf).
>  [ISMIR 2013](http://dblp1.uni-trier.de/db/conf/ismir/ismir2013.html#PachetSM13): 275-280

cf. Simon Dixon: _François Pachet has the definitive dataset for jazz, but it isn't public__



## FiloBass

- 48 manually verified transcriptions of professional jazz bassists
- 50 000 notes

sources:
- https://aim-qmul.github.io/FiloBass/
- https://arxiv.org/abs/2311.02023
- http://www.eecs.qmul.ac.uk/~simond/pub/2023/RileyDixon-ISMIR2023.pdf

content:
- audio stems, 
- scores, 
- performance-aligned MIDI 
- metadata:
	- beats, 
	- downbeats, 
	- chord symbols 
	- markers for musical form.



## FiloSax

- 48 tracks, 24 hours of audio

sources:
- https://dave-foster.github.io/filosax/
- https://zenodo.org/records/6335779
- https://zenodo.org/records/5625643 (pdf)
- https://www.eecs.qmul.ac.uk/~simond/pub/2021/FosterDixon-Filosax-ISMIR2021.pdf

content:
- MIDI files 
- MusicXML files



## Musescore Fake Real Book

MusicXML lead sheets from MuseScore

- olof29: 
  https://musescore.com/user/14854046/sheetmusic

  - > 440 pages

  - classement alphabétique

content:

- Musescore files
- MusicXML files

see also

- JAZZVAR below
- https://99percentinvisible.org/episode/the-real-book/
- https://www.swiss-jazz.ch/partitions-real-book.htm (pdf)



## JSD

Jazz Structure Dataset
- 340 recordings 
- > 3000 annotated segments
- segment-wise encoding of the solo and accompanying instruments

> Stefan Balke, Julian Reck, Christof Weiß, Jakob Abeßer, Meinard Müller
> JSD: A Dataset for Structure Analysis in Jazz Music
> TISMIR 5.1, 2022

sources:
- https://transactions.ismir.net/articles/10.5334/tismir.131
- https://github.com/stefan-balke/jsd

content:
- annotations segments



## MedleyDB

- 200 tracks 
- variety of styles, only 10 appropriate jazz recordings

> R. M. Bittner, J. Wilkins, H. Yip, and J. P. Bello
> MedleyDB 2.0
> New data and a system for sustainable data collection
> in ISMIR Late Breaking and Demo Papers. ISMIR, 2016.

sources:

content:
- full mixes 
- separate instrumental/vocal stems 
- pitch annotations for the lead line



## Harte’s Beatles Chord Database

- the chords of all 180 songs featured on original Beatles studio albums

> C. Harte, M. Sandler, S. A. Abdallah, and E. Gomez
> Symbolic representation of musical chords: A proposed syntax for text annotations. 
> In Proc. ISMIR 2005, London, UK, 2005.

cited in (above ref):

> M. Mauch, S. Dixon, C. Harte, M. Casey, B. Fields
> [Discovering Chord Idioms Through Beatles and Real Book Songs](http://www.eecs.qmul.ac.uk/%7Esimond/pub/2007/ISMIR2007_p255_mauch.pdf).
> Proceedings of the [8th International Conference on Music Information Retrieval (ISMIR 2007)](http://ismir2007.ismir.net/)



## JAZZVAR
- MIDI dataset
- 502 pairs of Variation and Original MIDI segments
  original extracted from lead-sheet in MuseScore
  Variation extracted from audio recording (to MIDI)
- 22 jazz standards, 47 performances, and 35 pianists
- the authors have collected 234 jazz standards from the Fake Real Book


> JAZZVAR: A Dataset of Variations found within Solo Piano Performances of Jazz Standards for Music Overpainting
> https://arxiv.org/abs/2307.09670



## The Claude Debussy Solo Piano Corpus

https://infoscience.epfl.ch/record/303768

> DCML
> The diachronic development of Debussy's musical style: A corpus study with Discrete Fourier Transform. Humanities and Social Sciences Communications
>  https://doi.org/10.1057/s41599-023-01796-7



## Jazznet

- 162 520 labeled _fundamental jazz piano patterns_
	- chords
	- arpeggios
	- scales
	- chord progressions with their inversions
- scripts piano pattern generators    
    based on _Distance-Based Pattern Structures_ (DBPS)
- 26k hours of audio 
- size of 95GB

[![image](https://user-images.githubusercontent.com/16122125/196017322-80bc3fdb-ede1-409b-b71b-80860d4d629b.png)](https://user-images.githubusercontent.com/16122125/196017322-80bc3fdb-ede1-409b-b71b-80860d4d629b.png)

sources:
- https://github.com/tosiron/jazznet
- https://ieeexplore.ieee.org/abstract/document/10096620 (ICASSP 2023)
- background music theory: https://github.com/tosiron/jazznet/blob/main/musicBackground.md

content:
- MIDI files
- WAV files
- scripts