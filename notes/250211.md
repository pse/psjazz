# 11 février 2025

discussion téléphone 17:00

- Augustin 
- Florent



## Ton Index

importance de l'ordre des tons dans le ton index:
c'est le tie-break ultime pour l'estimation des tons locaux

l'ordre Ton  Index a été révisé (re-ordonnacement à la fermeture)
suivant un ordre (fixé) sur objets `Ton` 
lui même construit sur un ordre assez arbitraire des modes



- [ ] revoir avec

 - distance Weber modal blues 
 - pour ties: ordre lex KS, mode etc



**bug**

résultats diff spelling 1 table avec 30 et 165 tons?
diff globales sélectionnées



**Life cycle of Ton Index**
after creation, the ton index is open.
tons can be added with add() and global flag
at closure with close, tons are reordered…

after closure, no ton can be added

global flags can be changed with ...

`reset()` will restore global flags to thor state at closure



## Calcul grille

tendance à favoriser les tonalités en bémol dans Omnibook
en particulier dans le cas **d'enharmoniques**

exemples: 

- `PSE_165_AtbME_Gridx_Ad+TE_20250207-2050` /  `V3fYc` mesures 52, 53
  mesures entièrement bémolisées vs entièrement diésées dans original
  accords de Bmin et B

  ton estimées: Abmin et Cb 

  dans la grille, mesure 53, B et Cb ont tous les 2 cost = 0 
  → c'est la distance au précédent. qui départage, à l'avantage de Cb

- `V3fYc` mesure 144 
  idem



**deux options** 

1. import (dans speller) des noms de notes des accords écrits

= **spelling contraint**: si le nom de note est connu, c'est le seul choix possible.

c'est implémenté (commit `4d1b3387c8af72a63ae60da7a23724654a360aa6` branch `beta`)

c'est pertinent dans le cadre de la transcription d'improvisation sur standards.



2. `CostAD` où la distance est calculée par rapport aux accords

et pas par rapport à la tonalité locale dans la "grid" estimée

ça implique la définition d'une classe `Chord` + intégration dans `enum`



## Revision CostAT

- update des composantes de tie-breaks

- selection globale

- [ ] regarder avec `pdist` Cost lexico 




## Octaves 

question précédente des accidents à octaves différents:

possible mais cela implique une révision de `PSState`

- data: tableau de longueur 7 → matrice 7 $\times$ nb octaves
- 2 modes : avec ou sans octaves (flag member)
  avec valeur par défaut pour un nouvel acg. octave 
  dans `equal`,   `accid` ,  `update`, `member`

argument mode pour les appels dans

- PSC1 constructeur pour  `accid` ,  `update`, `member`
- PSC1== et autres??? pour `equal`  (`operator==` for queues etc)

puis 

- Speller `reval_table`



**exemple**:

`evalOmnibook/PSE_165_ATD_Gridx_AD+TD_20250131-1601/3zn4c`/78 Db → C# (a-1)

`1evalOmnibook/PSE_165_ATD_Gridx_AD+TD_20250131-1601/3zn4c`/110 F# - F



## Cb, B#, Fb, E#

soupçon qu'il n'y en pas dans l'**omnibook**

stats sur leur présence
- [ ] dans le texte
- [ ] dans les accords (Chord Symbol M21)

si oui c'est un choix arbitraire...
- [ ] option pour les interdire (si pas à la clé) ?



autre pb:
- [ ] supprimer les « other » dans accords



## Réfs

article transcription omnibook 

- https://arxiv.org/abs/2405.16687
- https://aim-qmul.github.io/SaxTranscriptionPipeline/

data

- https://zenodo.org/records/14628467

- [ ] vérifier qu'on a la version la plus récente des données



voir aussi



>  Uzay Bora, Baris Tekin Tezel, Alper Vahaplar
>  An algorithm for spelling the pitches of any musical scale
>  Information Sciences, vol. 472, 2019
>  https://doi.org/10.1016/j.ins.2018.09.015



